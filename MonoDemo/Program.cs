﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using Commons.Xml.Relaxng;
using Commons.Xml.Relaxng.Rnc;

namespace MonoDemo
{
    class Program
    {
        static void UseXml()
        {
            var errorCount = 0;
            try
            {
                var bookStore = new XmlTextReader("bookstore.xml");
                var bookStoreSchema = new XmlTextReader("bookstore.rng");
                var patterns = RelaxngPattern.Read(bookStoreSchema);

                var validator = new RelaxngValidatingReader(bookStore, patterns);

                // iterate through the Xml
                while (validator.Read()) {}
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                errorCount = 1;
            }

            if (errorCount == 0)
                Console.WriteLine("Wohoo!");
        }

        static void UseCompact()
        {
            int errorCount = 0;
            try
            {
                var bookStore = new XmlTextReader("bookstore.xml");

                RelaxngValidatingReader validator;

                using (TextReader source = File.OpenText("bookstore.rnc"))
                {
                    var parser = new RncParser(new NameTable());
                    var pattern = parser.Parse(source);
                    validator = new RelaxngValidatingReader(bookStore, pattern);
                }

                while (validator.Read())
                {

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                errorCount = 1;
            }

            if (errorCount == 0)
                Console.WriteLine("Wohoo!");
        }
        static void Main(String[] args)
        {
            

            Console.Write("Use Xml for compact? Type 'x' for Xml and 'c' for Compact: ");
            var mode = Console.ReadKey();
            Console.Write("\n\n");

            switch (mode.Key)
            {
                case ConsoleKey.C:
                    UseCompact();
                    break;
                case ConsoleKey.X:
                    UseXml();
                    break;
            }

            Console.Read();
        }
    }
}
